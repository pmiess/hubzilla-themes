<?php
	
	$nav_bg = "#f8f8f8";
	$nav_bd = "#e7e7e7";
	$nav_gradient_top = "#fff";
	$nav_gradient_bottom = "#f8f8f8";
	$nav_active_gradient_top = "#e0e0eb";
	$nav_active_gradient_bottom = "#f3f3f3";
	//$nav_active_gradient_bottom = "#fff";
	$nav_icon_colour = "#777";
	//$nav_active_icon_colour = "#769a1e";
	$nav_active_icon_colour = "#88b540";
	$radius = "4";
	$banner_colour = "#777";
